import * as React from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { Router, Stack, Scene } from 'react-native-router-flux';
import {createStore, applyMiddleware} from redux;
import { Home, Chat } from './components';
import thunkMiddleware from 'redux-thunk'

const instructions = Platform.select({
  ios: `Press Cmd+R to reload,\nCmd+D or shake for dev menu`,
  android: `Double tap R on your keyboard to reload,\nShake or press menu button for dev menu`,
});

const store = createStore(rootReducer);

export default class App extends React.Component {

  render() {
    return (
      <Provider store={store}>

        <Router navigationBarStyle={styles.statusBar} titleStyle={styles.statusBar}>
          <Stack key="root">
            <Scene key="home" component={Home} title="Home" />
            <Scene key="chat" component={Chat} />
          </Stack>
        </Router>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  statusBar: {
    backgroundColor: 'orange',
    color: 'white'
  }
});
