import React from 'react';
import { StyleSheet, Text, TextInput, Button, View, SafeAreaView, FlatList } from 'react-native';
import moment from 'moment'

export class MessageItem extends React.Component {
    render() {
        const { message, user } = this.props;
        const isAuthor = user === message.author;

        return (

            <View style={[styles.root, {
                alignItems: isAuthor ? 'flex-end' : 'flex-start'
            }]}>

                    <View style={[styles.message, {
                        backgroundColor: isAuthor ? "#AEFF" : "#CCC",
                        borderBottomLeftRadius: isAuthor ? 16 : 0,
                        borderBottomRightRadius: isAuthor ? 0 : 16,
                    }]}>
                        <Text style={styles.author}>
                            {message.author}
                        </Text>
                        <Text style={styles.content}>
                            {message.content}
                        </Text>
                    </View>
                    <Text style={styles.timestamp}>
                    {moment(message.created_at).fromNow()}
                    </Text>

            </View>

        );
    }
}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        padding: 8,
        //flexDirection: 'row'
    },
    message: {
        padding: 8,
        backgroundColor: "#CCC",
        maxWidth: 250,
        borderRadius: 15,
    },
    author: {
        fontWeight: "bold"
    },
    timestamp: {
        color: "#555",
        fontSize: 11
    }

});