import PouchDB from 'pouchdb-react-native';


const initState = {
  user: null,
  room:null,
  message : [
    {_id:1, author:'author1',content:'hello1', create_at:new Date()},
    {_id:2, author:'author2',content:'hello2', create_at:new Date()},
    {_id:3, author:'author3',content:'hello3', create_at:new Date()}
  ]
}
export default function (state = initState, action) {
  switch(action.type) {
    case 'JOIN_REQUEST':
      return {
        ...state,
        user:action.user, 
        room:action.room
      }
    case 'JOIN_SUCCESS':
      return {
        ...state,
        messages:action.messages
      }
    case 'SEND_MESSAGE_REQUEST':
      return {
        ...state,
        messages: [
          action.message,
          ...state.messages
        ]
      }
    case 'SEND_MESSAGE_SUCCESS':
    case 'SYNC_SUCCESS':
      return {
        ...state,
        messages: action.messages
      }
    case 'JOIN_FAILURE':
      console.error(action.error);
      break
    default:
      return state;
  }
}
