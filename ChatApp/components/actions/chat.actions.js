import {chatService} from '../services'
import{ Actions as routerActions } from 'react-native-router-flux'

export const chatActions = {
  join,
  sendMessage
}

function join(user, room) {
  return dispatch => {
    dispatch(request(user,room));
    routerActions.chat({title: `Room: ${room}`});
    chatService.join(user, room, 
      messages => {dispatch(syncSuccess(messages));
      }
    )
    .then(message => {
      dispatch(success(messages))
    })
    .catch(error => {
      dispatch(failure(error))
    })
    ;

  }

  function request(user, room) { return {type: 'JOIN_REQUEST', user, room}}
  function success(message) { return {type: 'JOIN_SUCCESS', message}}
  function failure(error) { return {type: 'FAILURE', error}}
  function SyncSuccess(message) { return {type: 'SYNC_SUCCESS', message}}
}


function sendMessage(message) {
  return dispatch => {
    dispatch(request(message));
    chatService.sendMessage(message)
      .then(messages => {
        dispatch(success(messages));
      })
      .catch(error => {
        dispatch(failure(error))
      })
  }

  function request(message) {return { type: 'SEND_MESSAGE_REQUEST', message}}
  function success(messages) {return { type: 'SEND_MESSAGE_SUCCESS', messages}}
  function failure(error) {return { type: 'FAILURE', error}}
}