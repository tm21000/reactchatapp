import PouchDB from 'pouchdb-react-native';

const COUCHDB_URL = "http://gathor.org:5984/";

class Chat {
  user = '';
  room = '';
  db = null;
  message = [];

  joint(user, room) {
    this.user = user;
    this.room = room
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "")
      .toLowerCase()
    ;
    this.db = new PouchDB(this.room);

    this.db.sync(this.room, `${COUCHDB_URL}/${this.room}`, {
      live :true,
      retry : true
    })

    return this.listMessages();
  }

  listMessages() {
    return this.db.allDocs({
      includ_docs: true
    }).then(result =>
        this.messages=result.rows
          .map(r => r.doc)
          .sort((a,b) => a.created_at < b.created_at ? 1 : -1)
    )
  }

  sendMessage(message) {
    return this.delay(this.db.post(message).then(({id}) => 
      this.messages = [
        { ...message, _id: id}, 
          ...this.messages
      ]
    ));
  }

  handleSync(changes) {
    const { direction, change } = changes;
    const { docs: newMessage } = change;
    if(direction == 'push') {
      return this.messages;
    }
    return this.messages = [
      ...this.messages,
      ...newMessages
    ].sort((a,b) => a.created_at < b.created_at ? 1 : -1)
  }
  
  delay( cb ) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(cb);
        }, 1000);
    })
}

}

export const chatService = new Chat();