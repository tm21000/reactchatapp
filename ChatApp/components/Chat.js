import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, TextInput, Button, View, SafeAreaView, FlatList } from 'react-native';
import { MessageItem } from './';
import { chatActions } from '../actions'

@connect(({ chat }) => ({ chat }))

export class Chat extends React.Component {

    state = {
        newMessage: "",

    }

    handleSubmit = e => {
        const { chat, dispatch } = this.props;
        const { user } = chat;
        const { messages, newMessage } = this.state;
        dispatch(chatActions.sendMessage({
            author: user,
            content: newMessage,
            created_at: new Date()
        }))
        this.setState({
            newMessage: ""
        })
    }

    render() {
        const { chat } = this.props;
        const { newMessage } = this.state;
        const { user, messages } = chat;

        return (
            <SafeAreaView style={styles.root}>
                <FlatList inverted
                    data={messages}
                    renderItem={({ item }) =>
                        <MessageItem user={user} message={item} />
                    }
                    keyExtractor={item => item._id}
                    style={styles.messageList}
                />
                <View style={styles.messageComposer}>
                    <TextInput style={styles.messageInput} value={newMessage} onChangeText={newMessage => this.setState({ newMessage })} />
                    <Button
                        title="Send"
                        onPress={this.handleSubmit}
                        style={styles.sendBtn}
                        disabled={!newMessage}
                    />
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    root: {
        flex: 1,
    },
    messageList: {
        flex: 1,
    },
    messageComposer: {
        flex: 0,
        flexDirection: "row",
        padding: 4,
        borderTopWidth: 1,
        borderColor: "#CCC",
        backgroundColor: "#EEE"
    },
    messageInput: {
        flex: 1,
        backgroundColor: "white",
        paddingHorizontal: 4
    },
    sendBtn: {
        flex: 0,
    },
});